using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace WebGIS.GeoMath
{
    public class GeoMath
    {
        private const double PixelTileSize = 256d;
        private const double DegreesToRadiansRatio = 180d / Math.PI;
        private const double RadiansToDegreesRatio = Math.PI / 180d;

        //��������� ������������ ������� � ������� �������� �������������
        [SqlFunction]
        public static string GetQuadkey(double x1, double y1, double x2, double y2)
        {
            int zoom = 0;
            int p0_X, p0_Y, p1_X, p1_Y;
            string quadkey = string.Empty;

            int prev_zoom_X = 0, prev_zoom_Y = 0;

            while (zoom <= 17)
            {
                p0_X = getXTilePos(x1, zoom);
                p0_Y = getYTilePos(y1, zoom);
                p1_X = getXTilePos(x2, zoom);
                p1_Y = getYTilePos(y2, zoom);

                if (p0_X != p1_X || p0_Y != p1_Y)
                {
                    quadkey = TileXYToQuadKey(prev_zoom_X, prev_zoom_Y, zoom - 1);
                    break;
                }

                if (zoom == 17)
                {
                    quadkey = TileXYToQuadKey(p0_X, p0_Y, 17);
                }

                prev_zoom_X = p0_X;
                prev_zoom_Y = p0_Y;

                zoom = zoom + 1;
            }

            return quadkey;
        }

        #region point
        [SqlFunction]
        public static SqlGeometry GetIconEnvelope(double longitude, double latitude, int labelPosition, int width, int height, int zoom)
        {
            // �������� ����������� ������ �� �����
            int cpX, cpY;
            cpX = getPixelXPosFromLongitude(longitude, zoom);
            cpY = getPixelYPosFromLatitude(latitude, zoom);

            int offsetX = 0, offsetY = 0;
            switch (labelPosition)
            {
                case 0://Center
                    offsetX = (int)(width / 2);
                    offsetY = (int)(height / 2);
                    break;
                case 1://Left
                    offsetX = width;
                    offsetY = (int)(height / 2);
                    break;
                case 2://Right
                    offsetX = 0;
                    offsetY = (int)(height / 2);
                    break;
                case 3://Top
                    offsetX = (int)(width / 2);
                    offsetY = height;
                    break;
                case 4://Bottom
                    offsetX = (int)(width / 2);
                    offsetY = 0;
                    break;
                case 5://Bottom
                    offsetX = 0;
                    offsetY = 0;
                    break;
            }

            int leftTopX, leftTopY, rightBottomX, rightBottomY;
            leftTopX = cpX - offsetX;
            leftTopY = cpY - height + offsetY;
            rightBottomX = cpX + width - offsetX;
            rightBottomY = cpY + offsetY;

            double topLat, leftLon, bottomLat, rightLon;
            leftLon = getLongitudeFromPixelPos(leftTopX, zoom);
            bottomLat = getLatitudeFromPixelPos(rightBottomY, zoom);
            topLat = getLatitudeFromPixelPos(leftTopY, zoom);
            rightLon = getLongitudeFromPixelPos(rightBottomX, zoom);

            var geomBuilder = new SqlGeometryBuilder();
            geomBuilder.SetSrid((0));
            geomBuilder.BeginGeometry(OpenGisGeometryType.Polygon);
            geomBuilder.BeginFigure(leftLon, topLat);
            geomBuilder.AddLine(rightLon, topLat);
            geomBuilder.AddLine(rightLon, bottomLat);
            geomBuilder.AddLine(leftLon, bottomLat);
            geomBuilder.AddLine(leftLon, topLat);
            geomBuilder.EndFigure();
            geomBuilder.EndGeometry();

            return geomBuilder.ConstructedGeometry;
        }
        #endregion

        //��������� ������ �������� ����������� ���������
        #region polyline
        [SqlFunction(FillRowMethodName = "FillLineQuadkeyListRow", TableDefinition = "Quadkey string")]
        public static IEnumerable GetPolylineQuadkeyList_geo(SqlGeometry geoData, int zoom)
        {
            if (geoData.STGeometryType().Value == "MultiLineString")
            {
                int numGeom = geoData.STNumGeometries().Value;

                for (int i = 1; i <= numGeom; i++)
                {
                    var quadkeys = getLineStringQuadkeys(geoData.STGeometryN(i), zoom);
                    foreach (var quadkey in quadkeys)
                        yield return quadkey;
                }
            }
            else if (geoData.STGeometryType().Value == "LineString")
            {
                var quadkeys = getLineStringQuadkeys(geoData, zoom);
                foreach (var quadkey in quadkeys)
                    yield return quadkey;
            }
            else
            {
                yield return string.Empty;
            }
        }
        static IEnumerable getLineStringQuadkeys(SqlGeometry geoData, int zoom)
        {
            if (geoData.STGeometryType().Value != "LineString")
                yield return string.Empty;

            List<ss_Point> tiles = new List<ss_Point>();

            double x1, y1, x2, y2;
            string res = string.Empty;
            int p0_X = 0,
                p0_Y = 0,
                p1_X = 0,
                p1_Y = 0;

            int leftX = 0,
                rightX = 0,
                topY = 0,
                bottomY = 0;

            int edgeX = getXTilePos(179, zoom);
            int edgeY = getYTilePos(89, zoom);
            int tileX, tileY;

            for (int p = 1; p <= geoData.STNumPoints(); p++)
            {
                if (p != 1 && p == geoData.STNumPoints())
                    break;

                x1 = geoData.STPointN(p).STX.Value;
                y1 = geoData.STPointN(p).STY.Value;
                x2 = geoData.STPointN(p + 1).STX.Value;
                y2 = geoData.STPointN(p + 1).STY.Value;

                p0_X = getPixelXPosFromLongitude(x1, zoom);
                p0_Y = getPixelYPosFromLatitude(y1, zoom);
                p1_X = getPixelXPosFromLongitude(x2, zoom);
                p1_Y = getPixelYPosFromLatitude(y2, zoom);

                leftX = getXTilePos(x1, zoom);
                rightX = getXTilePos(x2, zoom);
                topY = getYTilePos(y1, zoom);
                bottomY = getYTilePos(y2, zoom);

                var line = new ss_Line(new ss_Point(p0_X, p0_Y), new ss_Point(p1_X, p1_Y));

                ss_Line.swap(leftX, rightX, out leftX, out rightX);
                ss_Line.swap(topY, bottomY, out topY, out bottomY);
                if (Math.Abs(rightX - leftX) >= 1)
                {
                    for (int x = leftX; x <= rightX; x++)
                    {
                        var vline = new ss_Line(new ss_Point(x * 256, -edgeY * 256), new ss_Point(x * 256, edgeX * 256));
                        var point = vline.STIntersect(line);

                        if (!point.NaN)
                        {
                            tileX = (int)((point.x - 1) / 256.0);
                            tileY = (int)(point.y / 256.0);
                            tiles.Add(new ss_Point(tileX, tileY));

                            tileX = (int)((point.x + 1) / 256.0);
                            tileY = (int)(point.y / 256.0);
                            tiles.Add(new ss_Point(tileX, tileY));
                        }
                    }
                }

                if (Math.Abs(bottomY - topY) >= 1)
                {
                    for (int y = topY; y <= bottomY; y++)
                    {
                        var hline = new ss_Line(new ss_Point(-edgeX * 256, y * 256), new ss_Point(edgeX * 256, y * 256));
                        var point = hline.STIntersect(line);

                        if (!point.NaN)
                        {
                            tileX = (int)(point.x / 256.0);
                            tileY = (int)((point.y - 1) / 256.0);
                            tiles.Add(new ss_Point(tileX, tileY));

                            tileX = (int)(point.x / 256.0);
                            tileY = (int)((point.y + 1) / 256.0);
                            tiles.Add(new ss_Point(tileX, tileY));
                        }
                    }
                }

                if (Math.Abs(rightX - leftX) == 0 && Math.Abs(topY - bottomY) == 0)
                {
                    bool flag = true;
                    foreach (var t in tiles)
                    {
                        if (t.x == leftX && t.y == topY)
                        {
                            flag = false;
                            break;
                        }
                    }

                    if (flag)
                    {
                        tiles.Add(new ss_Point(leftX, topY));
                    }
                }
            }


            for (int i = 0; i < tiles.Count; i++)
            {
                yield return TileXYToQuadKey((int)tiles[i].x, (int)tiles[i].y, zoom);
            }
        }
        public static void FillLineQuadkeyListRow(Object obj, out string Quadkey)
        {
            Quadkey = obj.ToString();
        }
        #endregion

        [SqlFunction]
        public static SqlGeometry GetQuadkeyGeometry(string quadkey)
        {
            if (string.IsNullOrEmpty(quadkey))
                return SqlGeometry.Null;

            int tileX, tileY, level;
            QuadKeyToTileXY(quadkey, out tileX, out tileY, out level);

            double top, left, right, bottom;

            left = getLongitudeFromTilePos(tileX, level);
            right = getLongitudeFromTilePos(tileX + 1, level);
            top = getLatitudeFromTilePos(tileY, level);
            bottom = getLatitudeFromTilePos(tileY + 1, level);

            var geomBuilder = new SqlGeometryBuilder();
            geomBuilder.SetSrid((0));
            geomBuilder.BeginGeometry(OpenGisGeometryType.Polygon);
            geomBuilder.BeginFigure(left, top);
            geomBuilder.AddLine(left, bottom);
            geomBuilder.AddLine(right, bottom);
            geomBuilder.AddLine(right, top);
            geomBuilder.AddLine(left, top);
            geomBuilder.EndFigure();
            geomBuilder.EndGeometry();
            return geomBuilder.ConstructedGeometry;
        }
        [SqlFunction]
        public static SqlGeometry GetQuadkeyGeometryExt(string quadkey, int ext = 0)
        {
            if (string.IsNullOrEmpty(quadkey))
                return SqlGeometry.Null;

            int tileX, tileY, level;
            QuadKeyToTileXY(quadkey, out tileX, out tileY, out level);

            double top, left, right, bottom;

            left = getLongitudeFromPixelPos(tileX * 256 - ext, level);
            right = getLongitudeFromPixelPos((tileX + 1) * 256 + ext, level);
            top = getLatitudeFromPixelPos(tileY * 256 - ext, level);
            bottom = getLatitudeFromPixelPos((tileY + 1) * 256 + ext, level);

            var geomBuilder = new SqlGeometryBuilder();
            geomBuilder.SetSrid((0));
            geomBuilder.BeginGeometry(OpenGisGeometryType.Polygon);
            geomBuilder.BeginFigure(left, top);
            geomBuilder.AddLine(left, bottom);
            geomBuilder.AddLine(right, bottom);
            geomBuilder.AddLine(right, top);
            geomBuilder.AddLine(left, top);
            geomBuilder.EndFigure();
            geomBuilder.EndGeometry();
            return geomBuilder.ConstructedGeometry;
        }

        #region private
        static int getXTilePos(double Longitude, int zoom)
        {
            var D = 128 * Math.Pow(2, zoom);
            var E = Math.Round(D + Longitude * 256 / 360 * Math.Pow(2, zoom), 0);
            int tileX = (int)Math.Floor(E / 256);

            return tileX;
        }
        static int getYTilePos(double Latitude, int zoom)
        {
            var D = 128 * Math.Pow(2, zoom);
            var A = Math.Sin(Math.PI / 180 * Latitude);
            var B = -0.9999;
            var C = 0.9999;

            if (A < B) A = B;
            if (A > C) A = C;
            var F = A;

            var G = Math.Round(D + 0.5 * Math.Log((1.0 + F) / (1.0 - F)) * (-256) * Math.Pow(2, zoom) / (2 * Math.PI), 0);
            int tileY = (int)Math.Floor(G / 256);

            return tileY;
        }
        static int getPixelXPosFromLongitude(double Longitude, int zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2.0, (double)zoomLevel);
            int x = (int)Math.Round((double)(Convert.ToSingle(pixelGlobeSize / 2d) + (Longitude * (pixelGlobeSize / 360d))));
            return x;
        }
        static int getPixelYPosFromLatitude(double Latitude, int zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2.0, (double)zoomLevel);
            var f = Math.Min(Math.Max(Math.Sin((double)(Latitude * RadiansToDegreesRatio)), -0.9999d), 0.9999d);
            int y = (int)Math.Round(Convert.ToSingle(pixelGlobeSize / 2d) + .5d * Math.Log((1d + f) / (1d - f)) * -(pixelGlobeSize / (2d * Math.PI)));
            return y;
        }
        static double getLongitudeFromTilePos(int XTilePos, int Zoom)
        {
            return (XTilePos / Math.Pow(2.0, Zoom) * 360.0 - 180.0);
        }
        static double getLatitudeFromTilePos(int YTilePos, int Zoom)
        {
            var n = Math.PI - 2.0 * Math.PI * YTilePos / Math.Pow(2.0, Zoom);
            return (180.0 / Math.PI * Math.Atan(0.5 * (Math.Exp(n) - Math.Exp(-n))));
        }
        static double getLongitudeFromPixelPos(int XTilePos, int Zoom)
        {
            return (XTilePos / (Math.Pow(2.0, Zoom) * 256) * 360.0 - 180.0);
        }
        static double getLatitudeFromPixelPos(int YTilePos, int Zoom)
        {
            var n = Math.PI - 2.0 * Math.PI * YTilePos / (Math.Pow(2.0, Zoom) * 256);
            return (180.0 / Math.PI * Math.Atan(0.5 * (Math.Exp(n) - Math.Exp(-n))));
        }
        static string TileXYToQuadKey(int tileX, int tileY, int levelOfDetail)
        {
            StringBuilder quadKey = new StringBuilder();
            for (int i = levelOfDetail; i > 0; i--)
            {
                char digit = '0';
                int mask = 1 << (i - 1);
                if ((tileX & mask) != 0)
                {
                    digit++;
                }
                if ((tileY & mask) != 0)
                {
                    digit++;
                    digit++;
                }
                quadKey.Append(digit);
            }
            return quadKey.ToString();
        }
        static void QuadKeyToTileXY(string quadKey, out int tileX, out int tileY, out int levelOfDetail)
        {
            tileX = tileY = 0;
            levelOfDetail = quadKey.Length;
            for (int i = levelOfDetail; i > 0; i--)
            {
                int mask = 1 << (i - 1);
                switch (quadKey[levelOfDetail - i])
                {
                    case '0':
                        break;

                    case '1':
                        tileX |= mask;
                        break;

                    case '2':
                        tileY |= mask;
                        break;

                    case '3':
                        tileX |= mask;
                        tileY |= mask;
                        break;

                    default:
                        throw new ArgumentException("Invalid QuadKey digit sequence.");
                }
            }
        }
        #endregion
    }
}
