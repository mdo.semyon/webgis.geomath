﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebGIS.GeoMath
{
    public class ss_Point
    {
        double _x;
        double _y;

        public double x { get { return _x; } }
        public double y { get { return _y; } }
        public bool NaN { get; set; }

        public ss_Point()
        {
            NaN = false;
        }

        public ss_Point(double x_, double y_)
        {
            _x = x_;
            _y = y_;
            NaN = false;
        }

        public static bool operator ==(ss_Point a, ss_Point b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(ss_Point a, ss_Point b)
        {
            return a.x != b.x || a.y != b.y;
        }

        public static bool operator <(ss_Point a, ss_Point b)
        {
            return a.x < b.x || Math.Abs(a.x - b.x) == 0 && a.y < b.y;
        }
        public static bool operator >(ss_Point a, ss_Point b)
        {
            return a.x > b.x || Math.Abs(a.x - b.x) == 0 && a.y > b.y;
        }

        public static ss_Point max(ss_Point a, ss_Point b)
        {
            if (a > b)
                return a;
            else
                return b;
        }

        public static ss_Point min(ss_Point a, ss_Point b)
        {
            if (a < b)
                return a;
            else
                return b;
        }
    }
}
