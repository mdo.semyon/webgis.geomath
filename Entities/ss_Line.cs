﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebGIS.GeoMath 
{
    public class ss_Line
    {
        ss_Point _p;
        ss_Point _q;

        double _a;
        double _b;
        double _c;
        public double A { get { return _a; } }
        public double B { get { return _b; } }
        public double C { get { return _c; } }

        public ss_Line(ss_Point p, ss_Point q)
        {
            _p = p;
            _q = q;
            _a = p.y - q.y;
            _b = q.x - p.x;
            _c = -_a * p.x - _b * p.y;
            norm();
        }

        public double dist(ss_Point p)
        {
            return _a * p.x + _b * p.y + _c;
        }

        public ss_Point STIntersect(ss_Line l)
        {
            ss_Point left = new ss_Point();
            ss_Point right = new ss_Point();
            right.NaN = true;

            var a = this._p;
            var b = this._q;
            var c = l._p;
            var d = l._q;

            if (!intersect_1d(a.x, b.x, c.x, d.x) || !intersect_1d(a.y, b.y, c.y, d.y))
            {
                //left = NaN;
                left.NaN = true;
                return left;
            }

            var m = new ss_Line(a, b);
            var n = new ss_Line(c, d);
            var zn = det(m.A, m.B, n.A, n.B);

            if (zn == 0)
            {
                if (Math.Abs(m.dist(c)) > 0 || Math.Abs(n.dist(a)) > 0)
                {
                    //left = NaN;
                    left.NaN = true;
                    return left;
                }

                if (b < a)
                {
                    swap(a, b, out a, out b);
                }

                if (d < c)
                {
                    swap(c, d, out c, out d);
                }

                //var left = ss_Point.max(a, c);
                left = ss_Point.max(a, c);
                right = ss_Point.min(d, b);
            }
            else
            {
                //var left = new ss_Point(-det(m.C, m.B, n.C, n.B) / zn, -det(m.A, m.C, n.A, n.C) / zn);
                left = new ss_Point(-det(m.C, m.B, n.C, n.B) / zn, -det(m.A, m.C, n.A, n.C) / zn);

                if (!betw(a.x, b.x, left.x) ||
                   !betw(a.y, b.y, left.y) ||
                   !betw(c.x, d.x, left.x) ||
                   !betw(c.y, d.y, left.y))
                {
                    //left = NaN;
                    left.NaN = true;
                }
            }

            return left;
        }

        void norm()
        {
            var z = Math.Sqrt(_a * _a + _b * _b);
            if (Math.Abs(z) > 0)
            {
                _a = _a / z;
                _b = _b / z;
                _c = _c / z;
            }
        }

        public static double det(double a, double b, double c, double d)
        {
            return a * d - b * c;
        }
        public static bool intersect_1d(double a, double b, double c, double d)
        {
            if (a > b)
            {
                swap(a, b, out a, out b);
            }

            if (c > d)
            {
                swap(c, d, out c, out d);
            }

            return Math.Max(a, c) <= Math.Min(b, d);
        }
        public static bool betw(double l, double r, double x)
        {
            if (l == r)
                return true;

            return Math.Min(l, r) <= x && x <= Math.Max(l, r);
        }
        public static void swap(double a, double b, out double newa, out double newb)
        {
            if (a > b)
            {
                newa = b;
                newb = a;
            }
            else
            {
                newa = a;
                newb = b;
            }
        }
        public static void swap(int a, int b, out int newa, out int newb)
        {
            if (a > b)
            {
                newa = b;
                newb = a;
            }
            else
            {
                newa = a;
                newb = b;
            }
        }
        public static void swap(ss_Point a, ss_Point b, out ss_Point newa, out ss_Point newb)
        {
            if (a.x > b.x)
            {
                newa = b;
                newb = a;
            }
            else
            {
                newa = a;
                newb = b;
            }
        }
    }
}
